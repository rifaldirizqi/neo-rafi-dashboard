import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fetchFrameList() {
  return dispatch => {
    const options = {
      method: 'get',
      url: SERVICES.GET_FRAME_LIST,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(listOFrameFetchedAction(res));
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(listOFrameFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}

export function fetchDeleteFrame(frameId) {
  return dispatch => {
    const options = {
      method: 'delete',
      url: SERVICES.DELETE_FRAME+frameId,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(deleteOFrameFetchedAction(res));
        dispatch(fetchFrameList());
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(deleteOFrameFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}

function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function listOFrameFetchedAction(data) {
  return {
    type: ACTIONS.LIST_OF_REWARD_FETCHED,
    data
  };
}

function deleteOFrameFetchedAction(data) {
  return {
    type: ACTIONS.DELETE_FRAME_FETCHED,
    data
  };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}