import React from 'react';
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import PromoForm from "components/forms/PromoForm";
import { ACTIONS } from 'constants/index';

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      render: false,
      image: null,
      data: null
    };
  }

  componentDidMount() {
    const { actions, match } = this.props;

    actions.fenchGetDetailPromo(match.params.id);
  }

  componentWillReceiveProps(nextProps) {
    let { DETAIL_OF_PROMO_FETCHED } = ACTIONS;
    let { type, data } = nextProps;
    if (type === DETAIL_OF_PROMO_FETCHED) {
      this.setState({render:true, data});
    }
  }

  _handleEditPromo = (data) => {
    data = Object.assign(data, {newsImage:this.state.image})
    let { actions } = this.props;
    actions.fetchPromoEdit(data);
  }

  _handleUpload = (image) => {
    this.setState({image});
  }

  render() {
    return (
      <Grid container>
        <GridItem xs={12} sm={12} md={8}>
          <Card>
            {this.state.render ?
              <PromoForm label="Edit Promo" onSubmit={(data) => { this._handleEditPromo(data) }} data={this.state.data} onUpload={(file) => this._handleUpload(file)} />
              :
              <div>loading...</div>
            }
          </Card>
        </GridItem>
      </Grid>
    );
  }

}

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
};