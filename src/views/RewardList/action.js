import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fetchRewardList() {
  return dispatch => {
    const options = {
      method: 'get',
      url: SERVICES.GET_REWARD_LIST,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(listORewardFetchedAction(res));
        dispatch(doneLoadingAction());
      })
      .catch((res) => {
        dispatch(listORewardFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}

function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function listORewardFetchedAction(data) {
  return {
    type: ACTIONS.LIST_OF_REWARD_FETCHED,
    data
  };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}