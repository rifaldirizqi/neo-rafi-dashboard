import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fetchFrameUpdate(data) {
  return dispatch => {
    const options = {
      method: 'put',
      url: SERVICES.EDIT_FRAME_BY_ID+data.frameId,
      data,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(createOfFrameFetchedAction(res));
        dispatch(doneLoadingAction());
        window.location.href = '/frame';
      })
      .catch(() => {
        dispatch(createOfFrameFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}
export function fetchFrameById(id) {
  return dispatch => {
    const options = {
      method: 'get',
      url: SERVICES.GET_FRAME_BY_ID+id,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(getOfFrameByIdFetchedAction(res));
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(getOfFrameByIdFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}


function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function createOfFrameFetchedAction(data) {
  return {
    type: ACTIONS.EDIT_FRAME_BY_ID,
    data
  };
}

function getOfFrameByIdFetchedAction(data) {
  return {
    type: ACTIONS.GET_OF_FRAME_BY_ID_FETCHED,
    data
  };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}