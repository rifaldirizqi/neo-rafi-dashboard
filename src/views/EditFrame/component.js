import React from 'react';
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import FrameForm from "components/forms/FrameForm";
import { ACTIONS } from 'constants/index';

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      render: false,
      image: null,
      data: null
    };
  }

  componentDidMount() {
    const { actions, match } = this.props;
    actions.fetchFrameById(match.params.id);
  }

  componentWillReceiveProps(nextProps) {
    let { GET_OF_FRAME_BY_ID_FETCHED } = ACTIONS;
    let { type, data } = nextProps;
    if (type === GET_OF_FRAME_BY_ID_FETCHED) {
      this.setState({render:true, data:data.data});
    }
  }

  _handleEditFrame = (data) => {
    data = Object.assign(data, {frameImage:this.state.image})
    let { actions } = this.props;

    actions.fetchFrameUpdate(data);
  }

  _handleUpload = (image) => {
    this.setState({image});
  }

  render() {
    return (
      <Grid container>
        <GridItem xs={12} sm={12} md={8}>
          <Card>
            {this.state.render ?
              <FrameForm label="Edit Frame" onSubmit={(data) => { this._handleEditFrame(data) }} data={this.state.data} onUpload={(file) => this._handleUpload(file)} />
              :
              <div>loading...</div>
            }
          </Card>
        </GridItem>
      </Grid>
    );
  }

}

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
};