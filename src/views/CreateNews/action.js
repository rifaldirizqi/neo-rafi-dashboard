import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fetchNewsCreate(data) {
  return dispatch => {
    const options = {
      method: 'post',
      url: SERVICES.ADD_NEWS,
      data,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(createOfNewsFetchedAction(res));
        dispatch(doneLoadingAction());
        window.location.href = '/news';
      })
      .catch(() => {
        dispatch(createOfNewsFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}


function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function createOfNewsFetchedAction(data) {
  return {
    type: ACTIONS.ADD_NEWS,
    data
  };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}