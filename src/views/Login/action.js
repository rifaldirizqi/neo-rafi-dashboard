import { ACTIONS } from 'constants/index';
import { setToken } from 'utils/common';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fetchLogin(data) {
  console.log(ACTIONS, SERVICES)
  return dispatch => {
    const options = {
      method: 'POST',
      url: SERVICES.LOGIN,
      data,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());
    fetch(options)
      .then(res => {
        console.log(res)
        let { _id, userId, username, email, name, mobileNumber, photoProfile } = res.data;
        let data = {
          _id,
          userId,
          username,
          email,
          name,
          mobileNumber,
          photoProfile
        }

        setToken(JSON.stringify(data));
        dispatch(loginAction(res));
        dispatch(doneLoadingAction());
        window.location.href = '/';
      })
      .catch((res) => {
        dispatch(doneLoadingAction());
        dispatch(showSnackbarAction({ message: res.message }));
      });
  };
}

function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function loginAction(data) {
  return {
    type: ACTIONS.LOGIN,
    data
  };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}


export function showSnackbarAction(data) {
  return {
    type: ACTIONS.SHOW_SNACKBAR,
    data,
  };
}

export function clearSnackbarAction() {
  return { type: ACTIONS.CLEAR_SNACKBAR };
}