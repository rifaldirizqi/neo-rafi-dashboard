import { ACTIONS } from 'constants/index';

const initialState = {
  data: [],
};

export default function reducer(state = initialState, action) {
  const { LOGIN } = ACTIONS;

  const { type, data } = action;

  switch (type) {
    case LOGIN:
      return {
        ...state,
        isLoading: false,
        data
      };
    default:
      return state;
  }
}
