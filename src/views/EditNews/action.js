import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fetchNewsEdit(data) {
  let { _id, ...value } = data;
  return dispatch => {
    const options = {
      method: 'put',
      url: SERVICES.EDIT_NEWS + '/' + _id,
      value,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(createOfNewsFetchedAction(res));
        dispatch(doneLoadingAction());
        window.location.href = '/news';
      })
      .catch(() => {
        dispatch(createOfNewsFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}

export function fenchGetDetailNews(id) {
  return dispatch => {
    const options = {
      method: 'get',
      url: `${SERVICES.GET_NEWS_DETAIL}/${id}`,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(rest => {
        dispatch(detailOfNewsFetchedAction(rest));
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(detailOfNewsFetchedAction({}));
        dispatch(doneLoadingAction());
      });
  };
}

function detailOfNewsFetchedAction(data) {
  return {
    type: ACTIONS.DETAIL_OF_NEWS_FETCHED,
    data
  };
}

function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function createOfNewsFetchedAction(data) {
  return {
    type: ACTIONS.EDIT_NEWS,
    data
  };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}