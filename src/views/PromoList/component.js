import React from "react";
import PropTypes from 'prop-types';
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableFooter from "@material-ui/core/TableFooter";
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Close from "@material-ui/icons/Close";
import InfoIcon from '@material-ui/icons/Info';
import Edit from "@material-ui/icons/Edit";
import NoteAdd from "@material-ui/icons/NoteAdd";
import { ROUTES } from 'configs';
import UltimatePaging from 'components/forms/UltimatePaging';
import { Link } from 'react-router-dom';
import { ACTIONS } from 'constants/index';
import Modal from 'components/Modal/Modal.jsx';

export default class Component extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      open: false,
      name: '',
      id: null,
      render: false
    }
    this._closeModal = this._closeModal.bind(this);
    this._handleDeletePromo = this._handleDeletePromo.bind(this);
  }

  componentDidMount() {
    const { actions } = this.props;

    actions.fetchPromoList();

  }

  componentWillReceiveProps(nextProps) {
    let { LIST_OF_PROMO_FETCHED } = ACTIONS;
    let { type } = nextProps;

    if (type === LIST_OF_PROMO_FETCHED) {
      this.setState({render: true});
    }
  }

  _handleDeletePromo = () => {
    const { actions } = this.props;
    actions.fetchPromoDelete(this.state.id);

    this._closeModal();
  }

  _openModal(name, id) {
    this.setState({
      open: true,
      name,
      id
    });
  }

  _closeModal() {
    this.setState({
      open: false,
      name: '',
      id: null
    });
  }

  _renderModal() {
    let { classes } = this.props;
    return (
      <Modal open={this.state.open} closeModal={this._closeModal} onClick={this._handleDeletePromo}>
        <div>
          <h6 className={classes.modalTitle}>
            {
              'Delete:' + this.state.name
            }
          </h6>
        </div>
      </Modal>
    );
  }
  _renderTable() {
    const { classes, data } = this.props;

    if (!this.state.render) {
      return (
        <h1>TIDAK ADA DATA</h1>
      );
    } else {
      return (
        <GridList cellHeight={160} className={classes.gridList} cols={3}>
          {
            data &&
              (
                data.map((tile, index) => (
                <GridListTile key={index} cols={tile.cols || 1}>
                  <img src={tile.listNewsPromoImage[0]} alt={tile.listNewsPromoName} />
                  <GridListTileBar
                    title={tile.listNewsPromoName}
                    subtitle={<span>category: {tile.listNewsCategory}</span>}
                    actionIcon={
                      <div>
                        <Link to={ROUTES.DETAIL_PROMO(tile._id)}>
                          <IconButton
                            aria-label="Close"
                            className={classes.tableActionButton}
                          >
                            <InfoIcon
                              className={
                                classes.tableActionButtonIcon + " " + classes.edit
                              }
                            />
                          </IconButton>
                        </Link>
                        <Link to={ROUTES.EDIT_PROMO(tile._id)}>
                          <IconButton
                            aria-label="Close"
                            className={classes.tableActionButton}
                          >
                            <Edit
                              className={
                                classes.tableActionButtonIcon + " " + classes.edit
                              }
                            />
                          </IconButton>
                        </Link>
                        <IconButton
                          onClick={() => this._openModal(tile.listNewsPromoName, tile._id)}
                          aria-label="Close"
                          className={classes.tableActionButton}
                        >
                          <Close
                            className={
                              classes.tableActionButtonIcon + " " + classes.close
                            }
                          />
                        </IconButton>
                      </div>
                    }
                  />
                </GridListTile>
              ))
            )
          }
        </GridList>    

      );
    }
  }

  _renderTableMeta() {
    const { data, page } = this.props;

    if (data.data && data.data.meta) {
      return (
        <TableFooter>
          <TableRow>
            <TableCell colSpan={4}>
              <UltimatePaging
                totalPages={(data.data.meta.totalPages !== undefined) 
                  ? data.data.meta.totalPages : 1}
                currentPage={page}
                onChange={this.onPageChange}
                />
            </TableCell>
          </TableRow>
        </TableFooter>
      );
    } else {
      return (<TableFooter />);
    }
  }

  render() {
    const { isLoading, classes } = this.props;

    return (
      <Grid container>
        <GridItem xs={12} sm={12} md={12}>
          <Card plain>
            <CardHeader plain color="primary">
              <Grid container>
                <GridItem xs={8} sm={6} md={6}>
                  <h4 className={classes.cardTitleWhite}>Promo </h4>
                </GridItem>
                <GridItem xs={4} sm={6} md={6}>
                  <div className={classes.side}>
                    <Link to={ROUTES.CREATE_PROMO()}>
                      <IconButton
                        aria-label="Close"
                        className={classes.tableActionButton}
                      >
                        <NoteAdd
                          className={
                            classes.tableActionButtonIcon + " " + classes.download
                          }
                        />
                      </IconButton>
                    </Link>
                  </div>
                </GridItem>
              </Grid>
            </CardHeader>
            <CardBody>
              {isLoading ? <h6>Loading..</h6> : this._renderTable() }
            </CardBody>
          </Card>
        </GridItem>
        {this._renderModal()}
      </Grid>
    );
  }
}
Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
  data: PropTypes.array,
  isLoading: PropTypes.bool,
  page: PropTypes.number,
  count: PropTypes.number
};