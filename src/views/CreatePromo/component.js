import React from 'react';
import PropTypes from "prop-types";
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import Card from "components/Card/Card.jsx";
import PromoForm from "components/forms/PromoForm";

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      render: false,
      image: null
    };
  }

  _handleCreatePromo = (data) => {
    data = Object.assign(data, {newsImage:this.state.image})
    let { actions } = this.props;
    actions.fetchNewsCreate(data);
  }

  _handleUpload = (image) => {
    this.setState({image});
  }

  render() {
    return (
      <Grid container>
        <GridItem xs={12} sm={12} md={8}>
          <Card>
            <PromoForm label="Create new Promo" onSubmit={(data) => { this._handleCreatePromo(data) }} onUpload={(file) => this._handleUpload(file)} />
          </Card>
        </GridItem>
      </Grid>
    );
  }

}

Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
};