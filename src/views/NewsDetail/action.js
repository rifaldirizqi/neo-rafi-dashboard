import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fenchGetDetailNews(id) {
  return dispatch => {
    const options = {
      method: 'get',
      url: `${SERVICES.GET_NEWS_DETAIL}/${id}`,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(rest => {
        dispatch(detailOfNewsFetchedAction(rest));
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(detailOfNewsFetchedAction({}));
        dispatch(doneLoadingAction());
      });
  };
}

function detailOfNewsFetchedAction(data) {
  return {
    type: ACTIONS.DETAIL_OF_NEWS_FETCHED,
    data
  };
}

function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}
