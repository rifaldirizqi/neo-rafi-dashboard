import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fenchGetDetailPromo(id) {
  return dispatch => {
    const options = {
      method: 'get',
      url: `${SERVICES.GET_PROMO_DETAIL}/${id}`,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(rest => {
        dispatch(detailOfPromoFetchedAction(rest));
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(detailOfPromoFetchedAction({}));
        dispatch(doneLoadingAction());
      });
  };
}

function detailOfPromoFetchedAction(data) {
  return {
    type: ACTIONS.DETAIL_OF_PROMO_FETCHED,
    data
  };
}

function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}
