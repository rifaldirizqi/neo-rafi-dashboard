import { ACTIONS } from 'constants/index';

const initialState = {
  data: {}
};

export default function reducer(state = initialState, action) {
  const { LIST_OF_REDEEM_REWARD_FETCHED } = ACTIONS;
  const { type, data, page, count } = action;

  switch (type) {
    case LIST_OF_REDEEM_REWARD_FETCHED:
      return {
        ...state,
        isLoading: false,
        data,
        page,
        count,
        type
      };

    default:
      return state;
  }
}
