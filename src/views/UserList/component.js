import React from "react";
import PropTypes from 'prop-types';
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import Table from "components/Table/Table.jsx";
import TableRow from "@material-ui/core/TableRow";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableFooter from "@material-ui/core/TableFooter";
import IconButton from "@material-ui/core/IconButton";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import { Link } from 'react-router-dom';
import Tooltip from "@material-ui/core/Tooltip";
import Edit from "@material-ui/icons/Edit";
import Close from "@material-ui/icons/Close";
import { ROUTES } from 'configs';
import UltimatePaging from 'components/forms/UltimatePaging';
import { ACTIONS } from 'constants/index';

export default class Component extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      render:false
    }
  }

  componentDidMount() {
    const { actions, page } = this.props;

    actions.fetchUserList({ page, count: 10 });

    this.onPageChange = this.onPageChange.bind(this);

  }

  componentWillReceiveProps(nextProps) {
    let { LIST_OF_USER_FETCHED } = ACTIONS;

    let { type } = nextProps;

    if( type === LIST_OF_USER_FETCHED ) {
      this.setState({render: true})
    }
  }

  onPageChange(page) {
    let { actions } = this.props;
    let { meta } = this.props.data.data;

    this.setState({ page });
    actions.fetchUserList({ page, count: meta.size });
  }

  handleDeleteUser = (userId) => {
    let { actions } = this.props;
    actions.fetchDeleteUser(userId);
  }

  _renderTable() {
    const { data } = this.props;

    if(!this.state.render) {
      return (
        <h1>TIDAK ADA DATA</h1>
      );
    } else {
      return (
        <Table
          tableHeaderColor="primary"
          tableHead={["No", "Nama", "E-mail", "Action"]}
        >
          <TableBody>
          {data.data && data.data.users &&
            (
              data.data.users.map((item, index) => this._renderTableRow(item, index + 1))
            )
          }
          </TableBody>
          {this._renderTableMeta()}
        </Table>
      );
    }
  }

  _renderTableRow(item, index) {
    let { _id, name, email, userId } = item;
    let { classes, page, count } = this.props;

    return (
      <TableRow key={_id}>
        <TableCell className={classes.tableCell}>{ ((page-1)* count)+ index}</TableCell>
        <TableCell className={classes.tableCell}>{name}</TableCell>
        <TableCell className={classes.tableCell}>{email}</TableCell>
        <TableCell className={classes.tableCell}>
          <Tooltip
            id="tooltip-top"
            title="User Detail"
            placement="top"
            classes={{ tooltip: classes.tooltip }}
          >
            <Link to={ROUTES.DETAIL_USER(userId)}>
              <IconButton
                aria-label="Edit"
                className={classes.tableActionButton}
              >
                <Edit
                  className={
                    classes.tableActionButtonIcon + " " + classes.edit
                  }
                />
              </IconButton>
            </Link>
          </Tooltip>
          <Tooltip
            id = "tooltip-top-start"
            title="User Delete"
            placement="top"
            classes={{ tooltip: classes.tooltip }}
          >
            <IconButton
              onClick={() => this.handleDeleteUser(userId)}
              aria-label="Close"
              className={classes.tableActionButton}
            >
              <Close
                className={
                  classes.tableActionButtonIcon + " " + classes.close
                }
              />
            </IconButton>
          </Tooltip>
        </TableCell>
      </TableRow>
    );
  }

  _renderTableMeta() {
    const { data, page } = this.props;

    if (data.data && data.data.meta) {
      return (
        <TableFooter>
          <TableRow>
            <TableCell colSpan={4}>
              <UltimatePaging
                totalPages={(data.data.meta.totalPages !== undefined) 
                  ? data.data.meta.totalPages : 1}
                currentPage={page}
                onChange={this.onPageChange}
                />
            </TableCell>
          </TableRow>
        </TableFooter>
      );
    } else {
      return (<TableFooter />);
    }
  }

  render() {
    const { isLoading, classes } = this.props;

    return (
      <Grid container>
        <GridItem xs={12} sm={12} md={12}>
          <Card>
            <CardHeader color="primary">
              <h4 className={classes.cardTitleWhite}>User</h4>
              <p className={classes.cardCategoryWhite}>
                Klik untuk mengetahui detail
              </p>
            </CardHeader>
            <CardBody>
              {isLoading ? <h6>Loading..</h6> : this._renderTable()}
            </CardBody>
          </Card>
        </GridItem>
      </Grid>
    );
  }

}
Component.propTypes = {
  actions: PropTypes.object,
  classes: PropTypes.object,
  data: PropTypes.object,
  isLoading: PropTypes.bool,
  page: PropTypes.number,
  count: PropTypes.number
};