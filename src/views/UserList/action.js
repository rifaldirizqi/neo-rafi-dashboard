import { ACTIONS } from 'constants/index';
import fetch from 'utils/fetch';
import { SERVICES } from 'configs';

export function fetchUserList(data) {
  return dispatch => {
    const options = {
      method: 'get',
      url: SERVICES.GET_USER_LIST+data.page+'/'+data.count,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(listOfUserFetchedAction(res,data.page, data.count));
        dispatch(doneLoadingAction());
      })
      .catch(() => {
        dispatch(listOfUserFetchedAction({}));
        dispatch(doneLoadingAction());
      });
  };
}


export function fetchDeleteUser(userId) {
  return dispatch => {
    const options = {
      method: 'delete',
      url: SERVICES.DELETE_USER+userId,
      headers: {
        Authorization: 'Basic dGVsa29tOmRhMWMyNWQ4LTM3YzgtNDFiMS1hZmUyLTQyZGQ0ODI1YmZlYQ==',
      }
    };

    dispatch(loadingAction());

    fetch(options)
      .then(res => {
        dispatch(deleteOfUserFetchedAction(res));
        dispatch(doneLoadingAction());
        dispatch(fetchUserList());
      })
      .catch(() => {
        dispatch(deleteOfUserFetchedAction([]));
        dispatch(doneLoadingAction());
      });
  };
}

function loadingAction() {
  return { type: ACTIONS.LOADING };
}

function listOfUserFetchedAction(data, page, count) {
  return {
    type: ACTIONS.LIST_OF_USER_FETCHED,
    data,
    page,
    count
  };
}

function deleteOfUserFetchedAction(data) {
  return {
    type: ACTIONS.DELETE_USER_FETCHED,
    data
  };
}

function doneLoadingAction() {
  return { type: ACTIONS.DONE_LOADING };
}