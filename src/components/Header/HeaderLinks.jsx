import React from "react";
import PropTypes from 'prop-types';
import withStyles from "@material-ui/core/styles/withStyles";
import Hidden from "@material-ui/core/Hidden";
import Lock from "@material-ui/icons/Lock";
import Button from "components/CustomButtons/Button.jsx";

import headerLinksStyle from "assets/jss/material-dashboard-react/components/headerLinksStyle";

class HeaderLinks extends React.Component {
  state = {
    open: false
  };

  handleClick = () => {
    this.setState({ open: !this.state.open });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, onClick } = this.props;

    return (
      <div>
        <Button
          color={"transparent"}
          justIcon={window.innerWidth > 959}
          simple={!(window.innerWidth > 959)}
          aria-label="Lock"
          className={classes.buttonLink}
          onClick={onClick}
        >
          <Lock className={classes.icons} />
          <Hidden mdUp>
            <p className={classes.linkText}>LOGOUT</p>
          </Hidden>
        </Button>
      </div>
    );
  }
}
HeaderLinks.propTypes = {
  classes: PropTypes.object,
  onClick: PropTypes.func,
};
export default withStyles(headerLinksStyle)(HeaderLinks);
