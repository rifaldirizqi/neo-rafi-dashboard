import React from "react";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Radio from "@material-ui/core/Radio";
import FiberManualRecord from "@material-ui/icons/FiberManualRecord";

const RadioCustom = (props) => {
  let { classes, labelText, value, name, selectedEnabled } = props;

  return (
    <div
      className={
        classes.checkboxAndRadio +
        " " +
        classes.checkboxAndRadioHorizontal
      }
    >
      <FormControlLabel
        control={
          <Radio
            checked={selectedEnabled}
            onChange={(data) => props.input.onChange(data)}
            value={value}
            name={name}
            aria-label={labelText}
            icon={
              <FiberManualRecord
                className={classes.radioUnchecked}
              />
            }
            checkedIcon={
              <FiberManualRecord className={classes.radioChecked} />
            }
            classes={{
              checked: classes.radio
            }}
          />
        }
        classes={{
          label: classes.label
        }}
        label={labelText}
      />
    </div>
  );
}

export default RadioCustom;