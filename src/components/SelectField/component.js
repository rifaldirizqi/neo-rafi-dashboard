import React from 'react';
import PropTypes from 'prop-types';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

export default class Component extends React.Component {

  _renderSelectOptions() {
    const { options } = this.props;

    return options.map((item, idx) => (
      <MenuItem key={idx} value={item.value}>
        {item.label}
      </MenuItem>
    ));
  }

  render() {
    const {
      input,
      label,
      meta: { error, touched },
    } = this.props;
    const helperText = (error && touched) ? error : '';

    return (
      <FormControl fullWidth margin="normal" error={error && touched}>
        <InputLabel>{label}</InputLabel>
        <Select
          {...input}
          error={error && touched}
          label={label}
        >
          {this._renderSelectOptions()}
        </Select>
        <FormHelperText>{helperText}</FormHelperText>
      </FormControl>
    );
  }
}

Component.propTypes = {
  classes: PropTypes.object,
  input: PropTypes.object,
  label: PropTypes.string,
  meta: PropTypes.object,
  options: PropTypes.array,
};