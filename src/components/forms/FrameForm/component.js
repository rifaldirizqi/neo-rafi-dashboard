import React from "react";
import PropTypes from "prop-types";
import { Field } from 'redux-form';
import Grid from "@material-ui/core/Grid";
import GridItem from "components/Grid/GridItem.jsx";
import Button from "components/CustomButtons/Button.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import TextField from "components/TextField/TextField.jsx";
import DropzoneInput from "components/DropzoneInput/DropzoneInput.jsx";

export default class Component extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      image: null,
    };
  }

  componentDidMount() {
    if (this.props.data !== null || this.props.data !== undefined) {
      this.props.initialize(this.props.data);
      this.setState({image:this.props.data.frameImage});
    }
  }

  handleChangeEnabled(event) {
    this.setState({selectedEnabled:event.target.value})
    console.log(event)
  }

  onUpload (data) {
    let { onUpload } = this.props;

    let file = data[0]
    let reader = new FileReader();
    reader.onload = (event) => {
      this.setState({image:event.target.result});
      onUpload(event.target.result);
    };
    reader.readAsDataURL(file);
  }

  render() {
    let { classes, handleSubmit, label } = this.props;

    return (
      <form onSubmit={handleSubmit} className={classes.form}>
        <CardHeader color="primary">
          <h4 className={classes.cardTitleWhite}>{label}</h4>
        </CardHeader>
        <CardBody>
          <Grid container>
            <GridItem xs={12} sm={12} md={12}>
              <Field 
                name="frameName"
                component={TextField}
                labelText="Name..."
                id="frameName"
                formControlProps={{
                  fullWidth: true
                }}
                type= "text"
              />
            </GridItem>
          </Grid>

          <Grid container>
            <GridItem xs={12} sm={12} md={6}>
              <h6 htmlFor={"frameImage"}>Frame Picture</h6>
              <Field
                name={"frameImage"}
                component={DropzoneInput}
                onChange={(data) =>{this.onUpload(data)}}
              />
              {this.state.image !== null ? <img src={this.state.image} width="100%" alt="framePreview" /> : null}
            </GridItem>
          </Grid>
        </CardBody>
        <CardFooter className={classes.cardFooter}>
          <Button type="submit" simple color="primary" size="lg">
            Submit
          </Button>
        </CardFooter>
      </form>
    );
  }
}
Component.propTypes = {
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
  submitting: PropTypes.bool,
  isLoading: PropTypes.bool,
  classes: PropTypes.object,
  data: PropTypes.object,
  label: PropTypes.string,
};