import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import withStyles from "@material-ui/core/styles/withStyles";
import Component from './component';
import styles from './styles';
import validate from './validate';

function mapStateToProps(state) {
  const { createFrame } = state.form;
  let values = {};
  
  if (createFrame && createFrame.values) values = createFrame.values;

  return {
    values
  };
}

function mapDispatchToProps() {
  return {
  };
}
const Styled = withStyles(styles)(Component);

const Connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(Styled);

export default reduxForm({
  form: 'frameForm',
  validate,
  initialValues: {
    frameName: ''
  },
  enableReinitialize: true
  // warn,
})(Connected);