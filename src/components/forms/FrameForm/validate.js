import validateInput from 'utils/validateInput';

const validate = values => {
  const {
    frameName,
  } = values;
  const errors = {
    frameName: validateInput(frameName, ['required']),
  };
  
  return errors;
};
 

export default validate;