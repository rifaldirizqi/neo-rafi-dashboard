import { createUltimatePagination, ITEM_TYPES } from 'react-ultimate-pagination';
import Page from './Page';
import Ellipsis from './Ellipsis';
import FirstPageLink from './FirstPageLink';
import PreviousPageLink from './PreviousPageLink';
import NextPageLink from './NextPageLink';
import LastPageLink from './LastPageLink';

const UltimatePaging = createUltimatePagination({
  itemTypeToComponent: {
    [ITEM_TYPES.PAGE]: Page,
    [ITEM_TYPES.ELLIPSIS]: Ellipsis,
    [ITEM_TYPES.FIRST_PAGE_LINK]: FirstPageLink,
    [ITEM_TYPES.PREVIOUS_PAGE_LINK]: PreviousPageLink,
    [ITEM_TYPES.NEXT_PAGE_LINK]: NextPageLink,
    [ITEM_TYPES.LAST_PAGE_LINK]: LastPageLink
  }
});

export default UltimatePaging;