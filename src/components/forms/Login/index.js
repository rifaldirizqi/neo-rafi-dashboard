import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import withStyles from "@material-ui/core/styles/withStyles";
import Component from './component';
import signupPageStyle from "assets/jss/material-dashboard-react/views/signupPageStyle.jsx";
import validate from './validate';

function mapStateToProps(state) {
  const { login } = state.form;
  let values = {};
  if (login && login.values) values = login.values;

  return {
    values
  };
}

function mapDispatchToProps() {
  return {
  };
}

const Styled = withStyles(signupPageStyle)(Component);

const Connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(Styled);

export default reduxForm({
  form: 'login',
  validate,
})(Connected);