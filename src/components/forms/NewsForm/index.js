import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import withStyles from "@material-ui/core/styles/withStyles";
import Component from './component';
import validate from './validate';
import styles from './styles';

function mapStateToProps(state) {
  const { createNews } = state.form;
  let values = {};
  
  if (createNews && createNews.values) values = createNews.values;

  return {
    values
  };
}

function mapDispatchToProps() {
  return {
  };
}
const Styled = withStyles(styles)(Component);

const Connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(Styled);

export default reduxForm({
  form: 'newsForm',
  validate,
  initialValues: {
    newsTitle: ''
  },
  enableReinitialize: true
  // warn,
})(Connected);