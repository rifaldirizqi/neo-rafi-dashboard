import validateInput from 'utils/validateInput';

const validate = values => {
  const {
    newsTitle,
    newsDescription,
    newsImage,
    newsLocation,
    newsAuthor,
    newsCategory,
  } = values;
  const errors = {
    newsTitle: validateInput(newsTitle, ['required', 'max-255']),
    newsDescription: validateInput(newsDescription, ['required']),
    newsImage: validateInput(newsImage, ['required']),
    newsAuthor: validateInput(newsAuthor, ['required', 'max-255']),
    newsLocation: validateInput(newsLocation, ['required', 'max-255']),
    newsCategory: validateInput(newsCategory, ['required']),
  };
  
  return errors;
};
 

export default validate;