import { connect } from 'react-redux';
import { reduxForm } from 'redux-form';
import withStyles from "@material-ui/core/styles/withStyles";
import Component from './component';
import styles from './styles';
import validate from './validate';

function mapStateToProps(state) {
  const { createPromo } = state.form;
  let values = {};

  if (createPromo && createPromo.values) values = createPromo.values;

  return {
    values
  };
}

function mapDispatchToProps() {
  return {
  };
}

const Styled = withStyles(styles)(Component);

const Connected = connect(
  mapStateToProps,
  mapDispatchToProps
)(Styled);

export default reduxForm({
  form: 'promoForm',
  validate,
  initialValues: {
    newsTitle: 'sate'
  },
  enableReinitialize: true
})(Connected);