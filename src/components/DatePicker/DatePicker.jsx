import React from "react";
import Datetime from "react-datetime";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import withStyles from "@material-ui/core/styles/withStyles";
import styles from './styles';

const DatePicker = (props) => {
  let { classes } = props;

  return (
    <div>
      <InputLabel className={classes.label}>
        Datetime Picker
      </InputLabel>
      <br />
      <FormControl fullWidth>
        <Datetime
          inputProps={{ placeholder: "Datetime Picker Here" }}
        />
      </FormControl>
    </div>
  );
}

export default withStyles(styles)(DatePicker);