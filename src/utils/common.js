import { TOKEN_STORAGE } from '../configs';

export function setToken(value) {
  localStorage.setItem(TOKEN_STORAGE, value);
}

export function getToken() {
  return localStorage.getItem(TOKEN_STORAGE);
}

export function clearToken() {
  localStorage.removeItem(TOKEN_STORAGE);
}