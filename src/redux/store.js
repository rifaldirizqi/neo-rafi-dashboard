import {
  createStore,
  applyMiddleware,
  compose
} from 'redux'
import thunkMiddleware from 'redux-thunk'
import createHistory from 'history/createBrowserHistory'
import {
  routerMiddleware
} from 'react-router-redux'
import reducer from './reducers'

export const history = createHistory()
const middleware = routerMiddleware(history)

export const configureStore = (initState) => {
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware,
      middleware
    ),
  )
  return createStore(reducer, initState, enhancer)
}

export const store = configureStore({})