import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import { reducer as formReducer } from 'redux-form';
import loading from './loading';
import login from 'views/Login/reducer';
import travelList from 'containers/TravelNews/reducer';
import culinaryList from 'containers/CulinaryNews/reducer';
import mudikList from 'containers/MudikInfo/reducer';
import routeList from 'containers/MudikRoute/reducer';
import userList from 'views/UserList/reducer';
import rewardList from 'views/RewardList/reducer';
import redeemList from 'views/RedeemList/reducer';
import frameList from 'views/FrameList/reducer';
import createFrame from 'views/CreateFrame/reducer';
import editFrame from 'views/EditFrame/reducer';
import promoList from 'views/PromoList/reducer';
import promoDetail from 'views/PromoDetail/reducer';
import createPromo from 'views/CreatePromo/reducer';
import editPromo from 'views/EditPromo/reducer';
import userDetail from 'views/UserDetail/reducer';
import newsDetail from 'views/NewsDetail/reducer';
import createNews from 'views/CreateNews/reducer';
import editNews from 'views/EditNews/reducer';

export default combineReducers(Object.assign({},
  {
    form: formReducer,
    login,
    travelList,
    culinaryList,
    mudikList,
    routeList,
    userList,
    rewardList,
    redeemList,
    createFrame,
    editFrame,
    frameList,
    promoList,
    promoDetail,
    createPromo,
    editPromo,
    userDetail,
    newsDetail,
    createNews,
    editNews,
    loading,
    routing: routerReducer
  },
));